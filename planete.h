//
// Created by Pierre  on 2019-04-30.
//
#ifndef VITESSE_SPATIALE_PLANETE_H
#define VITESSE_SPATIALE_PLANETE_H
#include <string>
#include <iostream>

class planete {

public:

    planete();
    planete(std::string NomPlanete, float X, float Y ,int POPULATION, std::string NATION, float PRIX);
    ~planete();
    void afficher() const;

private:
    std::string NomPlanete;
    float X;
    float Y;
    int POPULATION;
    std::string NATION;
    float PRIX;

};


#endif //VITESSE_SPATIALE_PLANETE_H
