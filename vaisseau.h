//
// Created by Pierre  on 2019-04-30.
//

#ifndef VITESSE_SPATIALE_VAISSEAU_H
#define VITESSE_SPATIALE_VAISSEAU_H
#include <string>
#include <iostream>

class vaisseau {
public:
    vaisseau(std::string Modele, int Capacite );
    ~vaisseau();
    void afficher();

private:
    std::string Modele;
    int Capacite;
};


#endif //VITESSE_SPATIALE_VAISSEAU_H
